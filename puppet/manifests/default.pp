include apt

# Sets up Nginx to proxy HTTP requests over port 80
# For additonal information see - https://github.com/jfryman/puppet-nginx

exec { "apt-get update":
  command => "/usr/bin/apt-get update"
}
Exec['apt-get update'] -> Package <| |>

# Install git
package { 'git-core': ensure => installed }

# Bring in the Odoo-specific configurations
class { 'odoo': }
class { 'odoo::ssh': }

class { 'odoo::source':
  repository_path   => $::application_root,
  repository_uri    => $::source_git_uri,
  repository_branch => $::source_branch,
  require => Class['odoo::ssh']
}

class{'odoo::database': }
class { 'odoo::startup':
  require => Class['odoo::database']
}
