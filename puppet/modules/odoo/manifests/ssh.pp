class odoo::ssh {

  file{'deploy':
    ensure => file,
    source => "puppet:///modules/odoo/deploy_key",
    path => "/home/vagrant/.ssh/deploy",
    mode => '0400',
    owner => 'vagrant',
    subscribe => Package['git-core']
  }

  exec{'eval':
    subscribe => File['deploy'],
    path => '/usr/local/bin/:/usr/bin/:/bin/',
    command => 'sh -c "eval `ssh-agent -s && ssh-add /home/vagrant/.ssh/deploy`"'
  }
}