class odoo::startup{

  file{'/etc/init/odoo.conf':
    ensure => file,
    source => 'puppet:///modules/odoo/odoo.conf',
  }

  service {'odoo':
    ensure => running,
    provider => 'upstart',
    subscribe => File['/etc/init/odoo.conf']
  }
}