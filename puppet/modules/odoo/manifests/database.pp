class odoo::database{
# Sets up the PostgreSQL database
# For additional configuration options - see https://forge.puppetlabs.com/puppetlabs/postgresql
  class { 'postgresql::server':
    listen_addresses  => '*',
    postgres_password => $::user_password,
    port              => $::postgresql_port
  }

  postgresql::server::role{ $::database_user :
    password_hash   => postgresql_password($::database_user, $::user_password),
    superuser => true
  }

# Enable to allow local pgsql commands
# class {'postgresql::client':}

  postgresql::server::db { $::application_database :
    user              => $::database_user,
    password          => $::user_password,
    encoding          => 'utf8',
    owner             => $::database_user,
  }

  postgresql::server::pg_hba_rule { 'allow application network to access app database':
    description => "Open up postgresql for access from VM host",
    type        => 'host',
    database    => $::application_database,
    user        => $::database_user,
    address     => 'all',
    auth_method => 'md5'
  }
}