class odoo::source(
  $repository_path,
  $repository_uri,
  $repository_branch
){
  vcsrepo { 'source':
    ensure    => present,
    path      => $repository_path,
    provider  => git,
    source    => $repository_uri,
    revision  => $repository_branch,
    owner     => 'vagrant',
    group     => 'vagrant',
    identity  => '/home/vagrant/.ssh/deploy',
    subscribe => Class['odoo::ssh'],
    notify    => Service['odoo']
  }
}
