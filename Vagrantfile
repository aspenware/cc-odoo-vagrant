# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

odoo_user_home = '/home/vagrant'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = 'hashicorp/precise64'

  # Time for SSH'ing into machine
  config.vm.boot_timeout = 1000

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.

  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.

  config.vm.network 'forwarded_port', guest: 8069, host: 8080 # forward the Odoo WSGI container to the host machine
  config.vm.network 'forwarded_port', guest: 5432, host: 8081 # forward the PostgreSQL TCP port to the host machine

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.

  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.

  # config.vm.network "public_network"

  # If true, then any SSH connections made will enable agent forwarding.
  # Default value: false

  # config.ssh.forward_agent = true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.

   config.vm.synced_folder './source', '/home/vagrant/odoo'

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   config.vm.provider 'virtualbox' do |vb|
  #   # Don't boot with headless mode
  #   vb.gui = true
  #
  #   # Use VBoxManage to customize the VM. For example to change memory:
     vb.customize ['modifyvm', :id, '--memory', '1024']
   end

  config.vm.provision 'shell', path: 'shell/bootstrap.sh'

  config.vm.provision 'puppet' do |puppet|
    puppet.manifests_path = 'puppet/manifests'
    puppet.manifest_file  = 'default.pp'
    puppet.module_path = 'puppet/modules'
    puppet.facter = {
        'application_root' => "#{odoo_user_home}/odoo",
        'source_git_uri' => 'git@bitbucket.org:aspenware/cc-odoo.git',
        'source_branch' => 'develop',
        'database_user' => 'odoo_user',
        'user_password' => 'p@s$w0rd!',
        'application_database' => 'odoo_db',
        'postgresql_port' => '5432'
    }
    # puppet.options << '--debug'
  end



end
