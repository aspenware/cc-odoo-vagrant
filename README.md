Odoo Vagrant Box
=================================

This project aims to provide a demonstration and development environment as a service by leveraging tools like Vagrant, VMWare, VirtualBox, and Vagrant. This particular vagrant box supports the [Odoo](https://www.odoo.com/) enterprise resource management tool.
The *basebox* VM image will have the essential run-time dependencies installed automatically for you and will expose a shared directory between host and guest where the source code for this application will live. The primary goal of this project is to abstract the run-time environment completely from the developer exposing only the database and the application through your local host.


VM Source
---------

This virtual machine is based off of the [Hashicorp Percise 64bit Ubuntu distribution](https://vagrantcloud.com/hashicorp/boxes/precise64).
It should support all of the major virtualization providers with the exception of Parallels.


Requirements
------------

In order to run this project, you will need to meet the following requirements:

* You have [Vagrant](https://www.vagrantup.com) installed on your local machine.
* Virtualization software such as Oracle's [VirtualBox](https://www.virtualbox.org/), VMWare's [Workstation](http://www.vmware.com/products/workstation), or [Hyper-V](http://www.microsoft.com/en-us/server-cloud/solutions/virtualization.aspx).

Optional Dependencies
---------------------

If you plan to use this Vagrant box for development, you may also need the following dependencies installed

* [Git](http://git-scm.com), A light-weight, distributed version control system
* A Python-friendly integrated development environment(IDE) ([Pycharm](w.jetbrains.com/pycharm/) is recommended)
* PostgreSQL database management software (integrated into Pycharm, otherwise [PgAdmin](http://www.pgadmin.org/) is another great solution)

Installation & Setup
--------------------

To install the Odoo box, simply clone the Git repository from [Bitbucket](https://bitbucket.org/aspenware/cc-odoo-vagrant/downloads). Once downloaded, your directory structure should look like this:

        .
        ├── README.md
        ├── Vagrantfile
        ├── puppet
        │   ├── manifests
        │   │   └── default.pp
        │   └── modules
        │       ├── apt
        │       ├── concat
        │       ├── nginx
        │       ├── odoo
        │       ├── postgresql
        │       ├── stdlib
        │       └── vcsrepo
        └── source

* The `source` directory will contain the Caption Colorado Odoo source code.
* The `puppet` directory contains configuration manifests and modules for the [Puppet](http://puppetlabs.com/puppet/puppet-open-source) provisioning framework.
* The `Vagrantfile` directs the configuration of the virtual machine through [Vagrant](https://www.vagrantup.com).

Once you've confirmed that your directory structure looks like this, open your terminal emulator (e.g. Terminal or Command Prompt) and type `vagrant up`

To specify your virtualizatioin provider, you will need to pass a commandline flag when you run vagrant. For example, in order to run Microsoft's Hyper-V, you would type:

        vagrant up --provider hyperv

The process of downloading, provisioning, and configuring your Vagrant development environment could take several minutes, especially over a slower network connection. Once your machine is up and running, you can connect to the Odoo web interface by visiting:

        http://localhost:8080

In your browser, and you can connect a PostgreSQL client at `localhost:8081`.

The credentials for the Odoo database are:

        Username: odoo_user
        Password: p@s$w0rd!
        Database: odoo_db

You can optionally restore the `odoo_db` from a development backup if you would like to start with some working data. To do that, download the [PostgreSQL backup file from here](https://intranet.aspenware.com/clients/captioncolo/Insights/Customer%20and%20Resource%20Management/aspenware9-9.backup).

You will need to use your PostgreSQL client to restore the database - we recommend [pgAdmin](http://www.pgadmin.org/).

Providers
---------

The Hashicorp Precise64 box supports, VirtualBox, VMWare Fusion, VMWare Workstation, and Hyper-V. For additional informaton on how to use
this box with various providers, please see the Vagrant documentation on the matter.

* [VirtualBox](https://docs.vagrantup.com/v2/virtualbox/index.html)
* [VMWare](https://docs.vagrantup.com/v2/vmware/index.html)
* [Hyper-V](https://docs.vagrantup.com/v2/hyperv/index.html)