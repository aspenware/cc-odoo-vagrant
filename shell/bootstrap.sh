#!/bin/bash -x

# This script ensures that the Puppet provisioning agent is installed on the guest machine
# so that the downstream puppet provisioners will run

if dpkg --get-selections | grep -q "^puppet[[:space:]]*install$" >/dev/null; then
    echo -e "puppet is already installed, skipping..."
else
    apt-get update
    apt-get -y install puppet
    echo "Successfully installed puppet"
fi
